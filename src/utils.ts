import { Session } from './session';

export function secondsToMs(seconds: number): number {
  return seconds * 1000;
}

export function genId(length: number): string {
  let result = '';
  const characters = '0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export async function withDelay<T>(fn: () => T | Promise<T>, ms: number) {
  await new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
  return fn();
}

export function getEnv(name: string): string {
  const result = process.env[name];
  if (result) {
    return result;
  }

  throw new Error(`No environment variable ${name} set!`);
}

export function handleProcessExit(session: Session) {
  // do something when app is closing
  process.on('exit', () => {
    session.disconnect();
  });

  // catches ctrl+c event
  process.on('SIGINT', () => {
    session.disconnect();
  });

  // catches "kill pid" (for example: nodemon restart)
  process.on('SIGUSR1', () => {
    session.disconnect();
  });
  process.on('SIGUSR2', () => {
    session.disconnect();
  });

  // catches uncaught exceptions
  process.on('uncaughtException', () => {
    session.disconnect();
  });
}
