export class ClientNotAuthorizedError extends Error {
  constructor(clientName: string) {
    super(`Client ${clientName} not authorized`);
  }
}
