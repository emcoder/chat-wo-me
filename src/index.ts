/* eslint-disable import/first */
import dotenv from 'dotenv';

dotenv.config();

import { runConversation } from './run-conversation';
import { rootLogger } from './logger';

runConversation()
  .then(() => rootLogger.info('Success'))
  .catch((err) => rootLogger.error(err));
