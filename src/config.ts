import { DeviceType } from './nekto';
import { getEnv } from './utils';

export const NEKTO_SOCKET_URL =
  process.env.NEKTO_SOCKET_URL || 'https://im.nekto.me';
export const NEKTO_SOCKET_TIMEOUT_SECONDS = 10; // 10s

export const CLIENT_2_SEARCH_TIMEOUT = 0.2; // 0.2s
export const TYPING_CHECK_INTERVAL = 3; // 3s
export const START_TYPING_DELAY = 1;

export const DIALOG_OPEN_TIMEOUT = 30;
export const DIALOG_MESSAGE_TIMEOUT = 120;

export const CLIENT_1_TOKEN = getEnv('CLIENT_1_TOKEN');
export const CLIENT_2_TOKEN = getEnv('CLIENT_2_TOKEN');

export const DEVICE_NAME = process.env.DEVICE_NAME || 'Android 10';
export const DEVICE_TYPE = process.env.DEVICE_TYPE || DeviceType.Android;
