import { ClientFactory } from './client';
import { CLIENT_1_TOKEN, CLIENT_2_TOKEN } from './config';
import { rootLogger } from './logger';
import { Session } from './session';
import { handleProcessExit } from './utils';

export async function runConversation() {
  const logger = rootLogger.child({ method: 'runConversation' });

  const [client1, client2] = [
    ClientFactory.buildWithToken('#1', CLIENT_1_TOKEN),
    ClientFactory.buildWithToken('#2', CLIENT_2_TOKEN),
  ];

  try {
    await Promise.all([client1.waitAuth(), client2.waitAuth()]);

    const session = new Session(rootLogger, client1, client2);
    session.runConversation();

    handleProcessExit(session);

    await Promise.all([client1.waitDisconnect(), client2.waitDisconnect()]);
  } catch (err) {
    client1.disconnect();
    client2.disconnect();
    logger.error(err as Error);
  }
}
