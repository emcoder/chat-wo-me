import { Client } from './client';
import { Dialog } from './client/dialog';
import { CLIENT_2_SEARCH_TIMEOUT } from './config';
import { ClientNotAuthorizedError } from './errors';
import { Logger } from './logger';
import { Message } from './nekto';
import { secondsToMs } from './utils';

export class Session {
  private readonly logger: Logger;

  constructor(
    logger: Logger,
    private readonly client1: Client,
    private readonly client2: Client,
  ) {
    this.logger = logger.child({ class: 'Session' });
  }

  private search() {
    this.client1.searchRun();
    // Second client search is started with delay
    // to prevent two clients connect to each other
    setTimeout(
      () => this.client2.searchRun(),
      secondsToMs(CLIENT_2_SEARCH_TIMEOUT),
    );
  }

  private leaveDialogs() {
    this.client1.leaveDialog();
    this.client2.leaveDialog();
  }

  private searchAfterDialogsClose() {
    const { client1, client2 } = this;

    client1.onceDialogClosed(() => {
      if (!client2.hasDialog()) {
        this.search();
      }
    });
    client2.onceDialogClosed(() => {
      if (!client1.hasDialog()) {
        this.search();
      }
    });
  }

  private processTyping() {
    const { client1, client2 } = this;

    client1.onTyping((typing) => this.handleTyping(client1, client2, typing));
    client2.onTyping((typing) => this.handleTyping(client2, client1, typing));
  }

  private handleTyping(
    typingClient: Client,
    interlocutor: Client,
    typing: boolean,
  ) {
    if (typing) {
      this.logger.info(`${typingClient.name} typing...`);
    }
    interlocutor.sendTyping(typing);
  }

  private handleDialogOpened(
    client: Client,
    interlocutor: Client,
    dialog: Dialog,
  ) {
    const logger = this.logger.child({ method: 'handleDialogOpened' });
    logger.info(`Dialog ${client.name} opened`);

    if (dialog.interlocutors.includes(interlocutor.id as number)) {
      logger.warn(
        `Client ${client.name} (id=${client.id}) ` +
          `connected with ${interlocutor.name} (id=${interlocutor.id})! ` +
          `Reload...`,
      );

      this.leaveDialogs();
      this.searchAfterDialogsClose();
    }
  }

  private openDialogs() {
    const { client1, client2 } = this;

    this.search();

    client1.onDialogOpened((dialog) =>
      this.handleDialogOpened(client1, client2, dialog),
    );

    client2.onDialogOpened((dialog) =>
      this.handleDialogOpened(client2, client1, dialog),
    );
  }

  private handleMessage(
    fromClient: Client,
    interlocutor: Client,
    data: Message,
  ) {
    const { message } = data;

    this.logger.info(`${fromClient.name}: %s`, message);

    interlocutor.onceDialog(() => {
      interlocutor.sendMessage(message);
    });
  }

  private processMessages() {
    const { client1, client2 } = this;

    client1.onMessage((data) => this.handleMessage(client1, client2, data));
    client2.onMessage((data) => this.handleMessage(client2, client1, data));
  }

  private processDialogClosed() {
    const { client1, client2 } = this;

    client1.onDialogClosed((dialog) => {
      this.handleDialogClosed(client1, client2, dialog);
    });
    client2.onDialogClosed((dialog) => {
      this.handleDialogClosed(client2, client1, dialog);
    });
  }

  private handleDialogClosed(
    closedClient: Client,
    interlocutor: Client,
    dialog: Dialog,
  ) {
    this.logger.info(`Dialog ${closedClient.name} was closed`);

    // Dialog with out own client was closed, it's ok
    if (interlocutor.id && dialog.interlocutors.includes(interlocutor.id)) {
      return;
    }

    this.disconnect();
  }

  private processError() {
    const { client1, client2 } = this;

    client1.onError((err) => this.handleError(err, client1, client2));
    client2.onError((err) => this.handleError(err, client2, client1));
  }

  private handleError(err: Error, errorClient: Client, _interlocutor: Client) {
    this.logger.error(`Client ${errorClient.name} error: %s`, err);

    this.disconnect();
  }

  private checkAuthorization() {
    const { client1, client2 } = this;

    if (!client1.isAuthorized()) {
      throw new ClientNotAuthorizedError(client1.name);
    }
    if (!client2.isAuthorized()) {
      throw new ClientNotAuthorizedError(client2.name);
    }
  }

  runConversation() {
    this.checkAuthorization();
    this.openDialogs();
    this.processTyping();
    this.processMessages();
    this.processDialogClosed();
    this.processError();
  }

  disconnect() {
    const logger = this.logger.child({ method: 'cleanUp' });
    logger.info('Disconnect clients...');

    this.client1.disconnect();
    this.client2.disconnect();
  }
}
