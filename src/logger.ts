import pino from 'pino';

export type Logger = pino.BaseLogger;

export const rootLogger: Logger = pino({
  level: process.env.LOG_LEVEL || 'info',
});
