import { EventEmitter } from 'events';
import io from 'socket.io-client';
import { NEKTO_SOCKET_TIMEOUT_SECONDS, NEKTO_SOCKET_URL } from '../config';
import { secondsToMs } from '../utils';
import { Logger, rootLogger } from '../logger';
import {
  NektoAction,
  NektoSocketEventType,
  Notice,
  NoticeHandler,
  NoticeType,
} from './types';

export enum SocketEvent {
  Disconnected = 'disconnected',
  Connected = 'connected',
  Error = 'error',
}

interface SocketEventHandlers {
  disconnected: Set<() => void>;
  error: Set<(err: Error) => void>;
}

export class NektoSocket {
  private readonly logger: Logger;

  private readonly socket: SocketIOClient.Socket;

  private readonly emitter: EventEmitter = new EventEmitter();

  private readonly noticeHandlers = new Map<NoticeType, Set<NoticeHandler>>();

  private readonly socketHandlers: SocketEventHandlers = {
    disconnected: new Set(),
    error: new Set(),
  };

  constructor(logger: Logger) {
    this.logger = logger.child({ class: 'NektoSocket' });
    this.socket = io.connect(NEKTO_SOCKET_URL, {
      autoConnect: false,
    });
  }

  connect() {
    const logger = this.logger.child({ method: 'connect' });
    const socket = this.socket;

    socket.on('connect_error', (err: Error) => {
      this.handleConnectError(err);
    });

    socket.on('connect', () => {
      this.handleConnect();
    });

    socket.on('error', (err: Error) => {
      this.handleSocketError(err);
    });

    socket.on('disconnect', () => {
      this.handleDisconnect();
    });

    socket.on('notice', (data: Notice) => {
      this.handleNotice(data);
    });

    logger.info(`Connecting to socket ${NEKTO_SOCKET_URL}...`);
    socket.connect();
  }

  isConnected(): boolean {
    return this.socket.connected;
  }

  disconnect() {
    this.socket.disconnect();
  }

  onError(fn: (err: Error) => void) {
    this.socketHandlers.error.add(fn);
  }

  onDisconnected(fn: () => void) {
    this.socketHandlers.disconnected.add(fn);
  }

  private handleConnect() {
    this.logger.info(`Connected to socket on ${NEKTO_SOCKET_URL}`);

    this.emitter.emit(NektoSocketEventType.Connected);
  }

  private handleDisconnect() {
    this.logger.warn('Socket disconnected');
    this.socketHandlers.disconnected.forEach((fn) => fn());
  }

  private handleConnectError(error: Error) {
    this.logger.error('Connect error: %s', error);
    this.socketHandlers.error.forEach((fn) => fn(error));
  }

  private handleSocketError(error: Error) {
    this.logger.error('Socket error: %s', error);
    this.socketHandlers.error.forEach((fn) => fn(error));
  }

  private handleNotice(message: Notice) {
    const logger = this.logger.child({ method: 'handleNotice' });
    const { notice, data } = message;

    logger.debug(`Notice received: %s`, notice);
    logger.trace('Notice data: %O', data);

    const handlers = this.noticeHandlers.get(notice);
    if (handlers && handlers.size > 0) {
      handlers.forEach((fn) => fn(data));
    }
  }

  private async waitConnected() {
    if (this.socket.connected) {
      return;
    }

    await new Promise<void>((resolve, reject) => {
      const timeout = setTimeout(
        () => reject(new Error('Socket connection timeout')),
        secondsToMs(NEKTO_SOCKET_TIMEOUT_SECONDS),
      );
      this.emitter.once(SocketEvent.Connected, () => {
        clearTimeout(timeout);
        resolve();
      });
    });
  }

  emitAction<T extends NektoAction>(action: T) {
    const logger = this.logger.child({ method: 'emitAction' });
    logger.debug(`Emit action ${action.action}...`);
    logger.trace(`Action data: %o`, action);

    this.waitConnected()
      .then(() => this.socket.emit('action', action))
      .catch((err) =>
        logger.error(`Nekto.me socket emit acton error: ${err.message}`),
      );
  }

  onNotice<T>(notice: NoticeType, fn: NoticeHandler<T>) {
    let handlers = this.noticeHandlers.get(notice);
    if (!handlers) {
      handlers = new Set();
      this.noticeHandlers.set(notice, handlers);
    }

    handlers.add(fn);
  }
}

export class NektoSocketFactory {
  static build() {
    const socket = new NektoSocket(rootLogger);
    socket.connect();
    return socket;
  }
}
