export type Sex = 'M' | 'F';
export type AgeRange = [18, 21] | [22, 25] | [26, 35] | [36, 100];

export interface SearchOptions {
  mySex?: Sex;
  wishSex?: Sex;
  myAge?: AgeRange;
  wishAge?: AgeRange[];
}

export enum ActionType {
  AuthGetToken = 'auth.getToken',
  AuthSendToken = 'auth.sendToken',
  AnonLeaveDialog = 'anon.leaveDialog',
  SearchRun = 'search.run',
  AuthSetFpt = 'auth.setFpt',
  CaptchaVerify = 'captcha.verify',
  AnonMessage = 'anon.message',
  SearchSendOut = 'search.sendOut',
  DialogSetTyping = 'dialog.setTyping',
  AnonReadMessages = 'anon.readMessages',
}

export enum NoticeType {
  AuthSuccessToken = 'auth.successToken',
  DialogClosed = 'dialog.closed',
  DialogOpened = 'dialog.opened',
  MessagesNew = 'messages.new',
  CaptchaVerify = 'captcha.verify',
  DialogTyping = 'dialog.typing',
  ErrorCode = 'error.code',
}

export enum NektoSocketEventType {
  Connected = 'connected',
  Error = 'error',
}

export interface NektoAction {
  action: ActionType;
}

export interface Notice<T = unknown> {
  notice: NoticeType;
  data: T;
}

export interface AuthSuccessTokenNoticeData {
  id: number;
  tokenInfo: {
    authToken: string;
    pushToken: string;
  };
  statusInfo: {
    anonDialogId: number | null;
  };
}

// eslint-disable-next-line
export type NoticeHandler<T = any> = (data: T) => void;

export interface Message {
  id: number;
  message: string;
  dialogId: number;
  senderId: number;
  randomId: string;
  createTime: number;
  isRead: boolean;
}

export interface DialogOpenedNoticeData {
  id: number;
  interlocutors: number[];
  messages: Message[];
  createTime: number;
  updateTime: number;
  close: number | null;
}

export interface TypingNoticeData {
  dialogId: number;
  typing: boolean;
}

export interface ErrorCodeNoticeData {
  id: number;
  description: string;
}

export enum DeviceType {
  Android = 1,
  Web = 2,
  IOS = 3,
}

export enum ErrorCode {
  CaptchaSolveRequired = 600,
  ChatAlreadyClosed = 205,
  LimitOfOpenChats = 200,
}
