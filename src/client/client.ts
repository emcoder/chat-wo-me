import { EventEmitter } from 'events';
import {
  AuthSuccessTokenNoticeData,
  NoticeType,
  Message,
  ActionType,
  SearchOptions,
  DialogOpenedNoticeData,
  TypingNoticeData,
  ErrorCodeNoticeData,
  NektoSocket,
  ErrorCode,
} from '../nekto';
import { Logger } from '../logger';
import { Dialog } from './dialog';
import { genId } from '../utils';
import { DEVICE_NAME, DEVICE_TYPE } from '../config';

export type MessageHandler = (data: Message) => void;
export type ErrorHandler = (err: Error) => void;
export type DialogOpenedHandler = (dialog: Dialog) => void;
export type DialogClosedHandler = DialogOpenedHandler;
export type TypingHandler = (typing: boolean) => void;

export enum ClientEvent {
  DialogOpened = 'dialog-opened',
  Authorized = 'authorized',
  Error = 'error',
  Disconnected = 'disconnected',
  DialogClosed = 'dialog-closed',
}

type ClientEventHandlers = {
  message: Set<MessageHandler>;
  dialogOpened: Set<DialogOpenedHandler>;
  dialogClosed: Set<DialogClosedHandler>;
  error: Set<ErrorHandler>;
  typing: Set<TypingHandler>;
};

const SAFE_ERRORS = [ErrorCode.ChatAlreadyClosed];

export class Client {
  private readonly logger: Logger;

  readonly name: string;

  private pushToken: string | null;

  private authToken: string | null;

  id: number | null;

  private deviceId: string | null;

  private sendCount = 0;

  private dialog: Dialog | null = null;

  private readonly emitter = new EventEmitter();

  private readonly handlers: ClientEventHandlers = {
    message: new Set(),
    dialogOpened: new Set(),
    dialogClosed: new Set(),
    error: new Set(),
    typing: new Set(),
  };

  disconnected = false;

  private deviceName = DEVICE_NAME;

  private deviceType = DEVICE_TYPE;

  constructor(
    logger: Logger,
    private readonly socket: NektoSocket,
    name: string,
    id: number | null,
    authToken: string | null,
    deviceId: string | null = null,
    pushToken: string | null = null,
  ) {
    this.logger = logger.child({ class: 'Client', deviceId });

    this.name = name;
    this.id = id;
    this.deviceId = deviceId;
    this.pushToken = pushToken;
    this.authToken = authToken;

    this.socket.onNotice<AuthSuccessTokenNoticeData>(
      NoticeType.AuthSuccessToken,
      (data) => this.handleAuthSuccessToken(data),
    );

    this.socket.onNotice<Message>(NoticeType.MessagesNew, (data) =>
      this.handleMessage(data),
    );

    this.socket.onNotice<DialogOpenedNoticeData>(
      NoticeType.DialogOpened,
      (data) => this.handleDialogOpened(data),
    );

    this.socket.onNotice(NoticeType.DialogClosed, () =>
      this.handleDialogClosed(),
    );

    this.emitter.on(ClientEvent.Error, (err: Error) => {
      this.handleError(err);
    });

    this.socket.onNotice<ErrorCodeNoticeData>(NoticeType.ErrorCode, (data) => {
      this.handleErrorCode(data);
    });

    this.socket.onNotice<TypingNoticeData>(NoticeType.DialogTyping, (data) => {
      this.handleTyping(data);
    });

    this.socket.onDisconnected(() => {
      this.handleDisconnected();
    });

    this.socket.onError((err) => {
      this.handleError(err);
    });
  }

  getDialog(): Dialog | null {
    return this.dialog;
  }

  onceDialog(fn: () => void) {
    if (this.dialog && !this.dialog.closed) {
      fn();
      return;
    }

    this.emitter.once(ClientEvent.DialogOpened, () => fn());
  }

  onceDialogClosed(fn: () => void) {
    if (!this.dialog || this.dialog.closed) {
      fn();
    }

    this.emitter.once(ClientEvent.DialogClosed, () => fn());
  }

  private handleErrorCode(data: ErrorCodeNoticeData) {
    this.logger.trace('Handle error message: %o', data);

    if (SAFE_ERRORS.includes(data.id)) {
      return;
    }

    const error = new Error(
      `Error notice ${data.id}: ${data.description || 'no description'}`,
    );
    this.emitter.emit(ClientEvent.Error, error);
  }

  private handleDialogClosed() {
    const { dialog } = this;

    if (dialog) {
      dialog.close();
      this.handlers.dialogClosed.forEach((fn) => fn(dialog));
      this.dialog = null;
    }
  }

  hasDialog(): boolean {
    return this.dialog !== null;
  }

  private handleDisconnected() {
    this.emitter.emit(ClientEvent.Disconnected);
  }

  onDialogClosed(fn: DialogClosedHandler) {
    this.handlers.dialogClosed.add(fn);
  }

  onDialogOpened(fn: DialogOpenedHandler) {
    this.handlers.dialogOpened.add(fn);
  }

  onTyping(fn: TypingHandler) {
    this.handlers.typing.add(fn);
  }

  async waitAuth() {
    if (this.isAuthorized()) {
      return;
    }

    await new Promise<void>((resolve) => {
      this.emitter.once(ClientEvent.Authorized, () => resolve());
    });
  }

  async waitDisconnect() {
    if (!this.socket.isConnected()) {
      return;
    }

    await new Promise<void>((resolve) => {
      this.emitter.once(ClientEvent.Disconnected, () => resolve());
    });
  }

  handleError(err: Error) {
    this.handlers.error.forEach((fn) => fn(err));
  }

  searchSendOut() {
    this.socket.emitAction({
      action: ActionType.SearchSendOut,
    });
  }

  private handleDialogOpened(data: DialogOpenedNoticeData) {
    const { id, interlocutors } = data;

    this.searchSendOut();

    if (!id) {
      this.emitter.emit(
        ClientEvent.Error,
        new Error(`Dialog open invalid data: ${JSON.stringify(data)}`),
      );
      return;
    }

    const dialog = new Dialog(id, interlocutors);
    this.dialog = dialog;

    this.handlers.dialogOpened.forEach((fn) => fn(dialog));
    this.emitter.emit(ClientEvent.DialogOpened);
  }

  private handleAuthSuccessToken(data: AuthSuccessTokenNoticeData) {
    const {
      id,
      tokenInfo: { authToken },
      statusInfo: { anonDialogId },
    } = data;

    this.id = id;
    this.authToken = authToken;

    this.emitter.emit(ClientEvent.Authorized);

    if (anonDialogId) {
      this.sendLeaveDialog(anonDialogId);
    }
  }

  isAuthorized(): this is Client & { id: number } {
    return Boolean(this.id && this.authToken);
  }

  private handleMessage(data: Message) {
    if (data.senderId === this.id) {
      return;
    }

    if (!this.dialog) {
      this.logger.warn('No dialog for received message: %o', data);
      return;
    }

    this.dialog.pushMessage(data);
    this.handlers.message.forEach((fn) => fn(data));

    this.socket.emitAction({
      action: ActionType.AnonReadMessages,
      dialogId: data.dialogId,
      lastMessageId: data.id,
    });
  }

  private handleTyping(data: TypingNoticeData) {
    this.handlers.typing.forEach((fn) => fn(data.typing));
  }

  auth() {
    if (!this.authToken) {
      throw new Error(`Auth without token not supported yet`);
    }

    const logger = this.logger.child({ method: 'authSendToken' });
    logger.debug('Send auth.sendToken & auth.setFpt...');

    this.socket.emitAction({
      action: ActionType.AuthSendToken,
      deviceId: this.deviceId,
      deviceName: this.deviceName,
      deviceType: this.deviceType,
      pushToken: this.pushToken,
      token: this.authToken,
    });
    // this.socket.emitAction({
    //   action: ActionType.AuthSetFpt,
    //   token: this.authToken,
    // });
  }

  searchRun(options: SearchOptions = {}) {
    this.socket.emitAction({
      action: ActionType.SearchRun,
      ...options,
    });
  }

  onError(fn: ErrorHandler) {
    this.handlers.error.add(fn);
  }

  onMessage(fn: MessageHandler) {
    this.handlers.message.add(fn);
  }

  sendMessage(message: string) {
    const logger = this.logger.child({ method: 'sendMessage' });

    const { dialog } = this;

    if (!dialog) {
      logger.error(
        `Can not send message: client (id=${this.id}) is not in dialog`,
      );
      return;
    }

    this.logger.debug(
      `Send ${message} from client (id=${this.id}) ` +
        ` on device (id=${this.deviceId}) ` +
        ` to dialog (id=${dialog.id}... (${this.sendCount})`,
    );

    this.socket.emitAction({
      action: ActionType.AnonMessage,
      dialogId: dialog.id,
      message,
      randomId: `${this.id}_${Number(new Date())}.${genId(16)}`,
    });
    this.sendCount += 1;
  }

  sendTyping(typing: boolean) {
    const { dialog } = this;

    if (dialog) {
      this.socket.emitAction({
        action: ActionType.DialogSetTyping,
        dialogId: dialog.id,
        typing,
      });
    }
  }

  leaveDialog() {
    const { dialog } = this;

    if (dialog && !dialog.closed) {
      this.sendLeaveDialog(dialog.id);
      dialog.closed = true;
    }
  }

  private sendLeaveDialog(dialogId: number) {
    this.socket.emitAction({
      action: ActionType.AnonLeaveDialog,
      dialogId,
    });
  }

  disconnect() {
    if (this.disconnected) {
      return;
    }

    this.logger.info(`Client ${this.name} disconnecting...`);

    this.socket.emitAction({
      action: ActionType.SearchSendOut,
    });

    this.leaveDialog();

    setTimeout(() => this.socket.disconnect(), 2000);

    this.disconnected = true;
  }
}
