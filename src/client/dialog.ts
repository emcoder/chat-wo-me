import { Message } from '../nekto';

export class Dialog {
  id: number;

  closed = false;

  messages: Message[] = [];

  interlocutors: number[];

  constructor(id: number, interlocutors: number[]) {
    this.id = id;
    this.interlocutors = interlocutors;
  }

  pushMessage(message: Message) {
    this.messages.push(message);
  }

  close() {
    this.closed = true;
  }
}
