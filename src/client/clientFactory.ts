import { Client } from './client';
import { NektoSocketFactory } from '../nekto';
import { rootLogger } from '../logger';

export class ClientFactory {
  static buildWithToken(name: string, authToken: string): Client {
    const socket = NektoSocketFactory.build();
    const client = new Client(rootLogger, socket, name, null, authToken);
    client.auth();
    return client;
  }
}
