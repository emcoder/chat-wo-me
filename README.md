# Chat without me
**Chat without me** connects two users of anonymous online chat nekto.me.

# Installation
1. `git clone git@gitlab.com:emcoder/chat-wo-me`
2. `cd chat-wo-me && npm i && npm run build`

# Configuration
You should obtain two different authorization tokens to open dialogs.  
To do so:

1. You should open browser
2. Open devtools
3. Open https://nekto.me/chat.
4. In devtools, find websocket connection, and retrive auth token from it logs.
5. You should start five different chats solving captcha. After that, nekto will not require to solve captcha anymore.
6. You should create `.env` file in project folder add token to it as `CLIENT_1_TOKEN=your-token`.
7. To get second token, you should repeat steps 2-5 in private window or another browser. Then copy second token to `.env` as `CLIENT_2_TOKEN`.

Sorry, there is no way in chat-wo-me yet to do it simplier. :(

# Running
Run `npm start` and see what happens. If errors occurs, app will close dialogs and exit.
